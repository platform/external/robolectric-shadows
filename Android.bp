// Copyright (C) 2019 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package {
    default_visibility: [":__subpackages__"],
    default_applicable_licenses: ["external_robolectric-shadows_license"],
}

// Added automatically by a large-scale-change that took the approach of
// 'apply every license found to every target'. While this makes sure we respect
// every license restriction, it may not be entirely correct.
//
// e.g. GPL in an MIT project might only apply to the contrib/ directory.
//
// Please consider splitting the single license below into multiple licenses,
// taking care not to lose any license_kind information, and overriding the
// default license using the 'licenses: [...]' property on targets as needed.
//
// For unused files, consider creating a 'fileGroup' with "//visibility:private"
// to attach the license to, and including a comment whether the files may be
// used in the current project.
// See: http://go/android-license-faq
license {
    name: "external_robolectric-shadows_license",
    visibility: [":__subpackages__"],
    license_kinds: [
        "SPDX-license-identifier-Apache-2.0",
        "SPDX-license-identifier-MIT",
    ],
    license_text: [
        "LICENSE",
    ],
}

// Empty library. Should be removed
java_library {
    name: "robolectric_android-all-stub",
}

//#############################################
// Assemble Robolectric_all
//#############################################

java_library_host {
    name: "Robolectric_all",

    static_libs: [
        "Robolectric_shadows_androidx_fragment",
        "Robolectric_shadows_httpclient",
        "Robolectric_shadows_framework",
        "Robolectric_shadows_supportv4",
        "Robolectric_shadows_multidex",
        "Robolectric_robolectric",
        "Robolectric_annotations",
        "Robolectric_resources",
        "Robolectric_shadowapi",
        "Robolectric_sandbox",
        "Robolectric_junit",
        "Robolectric_utils",
        "ow2-asm",
        "junit",
        "ow2-asm-tree",
        "guava",
        "ow2-asm-commons",
        "bouncycastle-unbundled",
        "robolectric-sqlite4java-0.282",
        "hamcrest",
        "hamcrest-library",
        "robolectric-host-androidx-test-runner",
        "robolectric-host-org_apache_http_legacy",
    ],

    java_resource_dirs: [
        "shadows/framework/src/main/resources",
        "src/main/resources",
    ],
}

// Make Robolectric_all available as a target jar
java_host_for_device {
    name: "Robolectric_all-target",
    libs: ["Robolectric_all"],
    visibility: [
      ":__subpackages__",
      //settings test
      "//packages/apps/TvSettings/Settings/tests/robotests:__pkg__",
      "//frameworks/base/packages/SettingsLib/tests/robotests:__pkg__",
      "//packages/apps/Settings/tests/robotests:__pkg__",
    ],
}

// Make dependencies available as host jars
java_device_for_host {
    name: "robolectric-host-androidx-test-core",
    libs: ["androidx.test.core"],
}

java_device_for_host {
    name: "robolectric-host-androidx-test-ext-junit",
    libs: ["androidx.test.ext.junit"],
}

java_device_for_host {
    name: "robolectric-host-androidx-test-monitor",
    libs: ["androidx.test.monitor"],
}

java_device_for_host {
    name: "robolectric-host-androidx-test-runner",
    libs: ["androidx.test.runner"],
}

java_device_for_host {
    name: "robolectric-host-androidx",
    libs: ["androidx.fragment_fragment"],
}

java_device_for_host {
    name: "robolectric-host-android-support-v4",
    libs: ["android-support-v4"],
}

java_device_for_host {
    name: "robolectric-host-android-support-multidex",
    libs: ["android-support-multidex"],
}

java_device_for_host {
    name: "robolectric-host-org_apache_http_legacy",
    libs: ["org.apache.http.legacy.stubs"],
}
