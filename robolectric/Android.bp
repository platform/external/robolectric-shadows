//#############################################
// Compile Robolectric robolectric
//#############################################

package {
    // See: http://go/android-license-faq
    // A large-scale-change added 'default_applicable_licenses' to import
    // all of the 'license_kinds' from "external_robolectric-shadows_license"
    // to get the below license kinds:
    //   SPDX-license-identifier-Apache-2.0
    default_applicable_licenses: ["external_robolectric-shadows_license"],
}

java_library_host {
    name: "Robolectric_robolectric",
    libs: [
        "Robolectric_shadows_framework",
        "Robolectric_annotations",
        "Robolectric_shadowapi",
        "Robolectric_resources",
        "Robolectric_sandbox",
        "Robolectric_junit",
        "Robolectric_utils",
        "robolectric-host-androidx-test-ext-junit",
        "robolectric-host-androidx-test-monitor",
        "robolectric-maven-ant-tasks-2.1.3",
        "bouncycastle-unbundled",
        "ow2-asm-commons",
        "guava",
        "robolectric-xstream-1.4.8",
        "ow2-asm-tree",
        "junit",
        "robolectric-ant-1.8.0",
        "ow2-asm",
        "jsr305",
        "robolectric-host-android_all",
    ],
    srcs: ["src/main/java/**/*.java"],

    java_resources: [":robolectric-version.properties"],
}

genrule {
    name: "robolectric-version.properties",
    out: ["robolectric-version.properties"],
    cmd: "echo -n 'robolectric.version=4.0-SNAPSHOT' > $(out)",
}

//#############################################
// Compile Robolectric robolectric tests
//#############################################
java_test_host {
    name: "Robolectric_robolectric_tests",
    srcs: ["src/test/java/**/*.java"],
    java_resource_dirs: ["src/test/resources"],
    static_libs: [
        "Robolectric_robolectric",
        "Robolectric_shadows_framework",
        "Robolectric_annotations",
        "Robolectric_shadowapi",
        "Robolectric_resources",
        "Robolectric_sandbox",
        "Robolectric_junit",
        "Robolectric_utils",
        "robolectric-host-androidx-test-ext-junit",
        "robolectric-host-androidx-test-monitor",
        "robolectric-host-androidx-test-core",
        "robolectric-maven-ant-tasks-2.1.3",
        "mockito",
        "bouncycastle-unbundled",
        "hamcrest",
        "robolectric-sqlite4java-0.282",
        "ow2-asm-commons",
        "robolectric-diffutils-1.3.0",
        "guava",
        "objenesis",
        "robolectric-xstream-1.4.8",
        "ow2-asm-tree",
        "junit",
        "icu4j",
        "truth",
        "robolectric-ant-1.8.0",
        "ow2-asm",
        "jsr305",
    ],
    libs: ["robolectric-host-android_all"],
    // Robolectric tests do not work well with unit tests setup yet
    test_options: {
        unit_test: false,
    },

    errorprone: {
        javacflags: [
            "-Xep:ReturnValueIgnored:WARN",
        ],
    },
}
